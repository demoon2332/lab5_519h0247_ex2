import '../models/data_layer.dart';
import '../services/category_services.dart';

class CategoryController{
  final services = CategoryServices();

  List<Category> get cates => List.unmodifiable(services.getAllCates());

  void addNewCate(String name){
    if(name.isEmpty)
      return;
    name = checkDuplicates(cates.map((e) => e.name), name);
    services.createCate(name);
  }

  void saveCate(Category c){
    services.saveCate(c);
  }

  void deleteCate(Category c){
    services.deleteCate(c);
  }

  Product createNewProduct(Category c){
    return services.addProduct(c);
  }

  void saveProduct(Category c,Product p){
    services.saveProduct(c,p);
  }

  void deleteProduct(Category c,Product p){
    services.deleteProduct(c, p);
  }


  String checkDuplicates(Iterable<String> items, String text) {
    final duplicatedCount = items.where((item) => item.contains(text)).length;
    if (duplicatedCount > 0) {
      text += ' (${duplicatedCount + 1})';
    }

    return text;
  }
}