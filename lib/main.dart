import 'package:flutter/material.dart';
import './provider/category_provider.dart';
import './views/app.dart';

void main(){
  var provider = CategoryProvider(child: App());
  runApp(provider);
}