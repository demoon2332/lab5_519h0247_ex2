import '../repositories/repository.dart';
import 'product.dart';

class Category{
  int id =0;
  String name = '';
  List<Product> products = [];

  Category({required this.id,this.name=''});

  Category.fromModel(Model model){
    id = model.id;
    name = model.data['name'] != null ? model.data['name'] : "unknown";
    if(model.data['products'] != null){
      products = model.data['products'].map<Product>((pro)=> Product.fromModel(pro)).toList();
    }
  }

  Model toModel()=> Model(id: id,data: {
    'name': name,
    'products': products.map((t) => t.toModel()).toList()
  });

  int get availableCount => products.where((t) => t.quantity > 0).length;

  String get availableMessage => 'Availble products: $availableCount out of ${products.length} products';
}