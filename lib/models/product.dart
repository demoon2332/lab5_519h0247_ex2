import '../repositories/repository.dart';

class Product {
  int id;
  String name;
  String description;
  int quantity;
  double price;

  Product({
    this.id=0,
    this.name = 'Unknown',
    this.description = '',
    this.quantity = 0,
    this.price = 0,
  });


  Product.fromModel(Model model)
      : id = model.id,
        name = model.data['name'],
        description = model.data['description'],
        quantity = model.data['quantity'],
        price =  model.data['price'];

  Model toModel()=>
      Model(id: id,data:{'name': name,'description':description,'quantity': quantity,'price': price});
}