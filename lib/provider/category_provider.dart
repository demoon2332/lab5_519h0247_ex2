import 'package:flutter/material.dart';
import '../controllers/category_controller.dart';

class CategoryProvider extends InheritedWidget {
  final _controller = CategoryController();

  CategoryProvider({Key? key, required Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget widget) => false;

  static CategoryController of(BuildContext context) {
    CategoryProvider provider = context
        .dependOnInheritedWidgetOfExactType<CategoryProvider>() as CategoryProvider;
    return provider._controller;
  }
}
