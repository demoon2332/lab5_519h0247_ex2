import 'package:l5_ex2/provider/category_provider.dart';

import '../models/data_layer.dart';
import '../repositories/in_cache_memory.dart';
import '../repositories/repository.dart';

class CategoryServices{
  Repository repo = InMemoryCache();

  Category createCate(String name){
    final model  = repo.create();
    final c = Category.fromModel(model)..name = name;
    saveCate(c);
    return c;
  }

  void saveCate(Category c){
    repo.update(c.toModel());
  }

  void deleteCate(Category c){
    repo.delete(c.toModel().id);
  }

  List<Category> getAllCates(){
    return repo.getAll()
        .map((e) => Category.fromModel(e))
        .toList();
  }

  Product addProduct(Category c){
    final id = c.products.length;
    final pro = Product(id: id,description: "",name: "Unknown",quantity: 0,price: 0);
    c.products.add(pro);
    saveCate(c);
    return pro;
  }

  void saveProduct(Category c,Product p){
    c.products[p.id] = p;
    repo.update(c.toModel());
  }

  void deleteProduct(Category c,Product p){
    c.products.remove(p);
    saveCate(c);
  }


}