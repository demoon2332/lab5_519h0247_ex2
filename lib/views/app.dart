

import 'package:flutter/material.dart';
import 'package:l5_ex2/views/home_screen.dart';

class App extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Tiki',
      home: Scaffold(
        body: HomeScreen(),
      ),
    );
  }
}