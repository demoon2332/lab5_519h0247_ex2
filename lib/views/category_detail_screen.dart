import 'package:l5_ex2/provider/category_provider.dart';
import '../models/data_layer.dart';
import 'package:flutter/material.dart';
import '../provider/category_provider.dart';

class ProductDetailScreen extends StatefulWidget {
  final Category cate;
  const ProductDetailScreen({Key? key, required this.cate}) : super(key: key);
  @override
  State createState() => ProductDetailState();
}

class ProductDetailState extends State<ProductDetailScreen> {
  late ScrollController scrollController;
  Category get cate => widget.cate;
  @override
  void initState() {
    super.initState();
    scrollController = ScrollController()
      ..addListener(() {
        FocusScope.of(context).requestFocus(FocusNode());
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(cate.name + ' Detail')),
        body: Column(
          children: <Widget>[
            Expanded(child: buildList()),
            SafeArea(
                child: Padding(
                    padding: EdgeInsets.fromLTRB(8, 4, 8, 6),
                    child: Text(cate.availableMessage)))
          ],
        ),
        floatingActionButton: buildAddTaskBtn());
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  Widget buildAddTaskBtn() {
    return FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () {
        final controller = CategoryProvider.of(context);
        Product newProduct = controller.createNewProduct(cate);
        cate.products.add(newProduct);
        setState(() {});
      },
    );
  }

  Widget buildList() {
    //final plan = PlanProvider.of(context);
    return Column(
      children: <Widget>[
          Padding(
              padding: EdgeInsets.fromLTRB(8, 8, 8, 10),
              child: Text(
                'You can add more products to this category ',
                style: TextStyle(fontSize: 12),
              )),
        Expanded(
          flex: 1,
            child: ListView.builder(
          controller: scrollController,
          itemCount: cate.products.length,
          itemBuilder: (context, index) => buildTaskTile(cate.products[index]),
        ))
      ],
    );
  }

  Widget buildTaskTile(Product p) {
    return Dismissible(
      key: ValueKey(p),
      background: Container(color: Colors.red),
      direction: DismissDirection.endToStart,
      onDismissed: (_) {
        final controller = CategoryProvider.of(context);
        controller.deleteProduct(cate, p);
        setState(() {});
      },
      child: Padding(
        padding: EdgeInsets.only(bottom: 10),
        child: ListTile(
          leading: Text(p.id.toString()+".",style: TextStyle(color: Colors.deepOrangeAccent,fontSize: 16)),
          title: TextFormField(
            decoration: InputDecoration(
              labelText: 'Name',
              hintText: "Product's name",
              labelStyle: TextStyle(
                  color: Colors.black54, fontSize: 16, fontFamily: 'AvenirLight'),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.indigoAccent),
              ),
            ),
            onFieldSubmitted: (desc) {
              setState(() {
                p.name = desc;
              });
            },
          ),
          subtitle: TextFormField(
            decoration: InputDecoration(
              labelText: 'Description',
              hintText: "Product's description",
              labelStyle: TextStyle(
                  color: Colors.black54, fontSize: 16, fontFamily: 'AvenirLight'),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.indigoAccent),
              ),
            ),
            initialValue: p.description,
            onFieldSubmitted: (desc) {
              setState(() {
                p.description = desc;
              });
            },
          ),
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(child: SizedBox(),flex: 1,),
              ElevatedButton(
                onPressed: () {
                  final controller = CategoryProvider.of(context);
                  controller.saveProduct(cate, p);
                },
                child: Text('Save',style: TextStyle(fontSize: 18,color: Colors.white),),
                style: ElevatedButton.styleFrom(
                    primary: Colors.purple,
                    padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                    textStyle: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold)),
              ),
              Expanded(child: SizedBox(),flex: 1,),
            ],
          )
        ),
      )
    );
  }
}
