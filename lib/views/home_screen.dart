
import 'package:flutter/material.dart';
import '../provider/category_provider.dart';
import 'category_detail_screen.dart';

class HomeScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen>{
  final categoryController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Tiki Store'),),
      body: Column(
          children: [
            buildList(),
            Expanded(child: buildDetailProduct())
          ]
      ),
    );
  }

  @override
  void dispose() {
    categoryController.dispose();
    super.dispose();
  }

  void addCategory() {
    final String text = categoryController.text;
    print("hehe:"+text);
    // All the business logic has been removed from this 'view' method!
    final controller = CategoryProvider.of(context);
    controller.addNewCate(text);

    categoryController.clear();
    FocusScope.of(context).requestFocus(FocusNode());
    setState(() {});
  }


  Widget buildList(){
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Material(
          child: Column(
            children: [
              TextField(
                controller: categoryController,
                decoration: InputDecoration(
                    labelText: 'Add a new plan'
                ),
              ),
              Padding(padding: EdgeInsets.all(8)),
              ElevatedButton(onPressed: addCategory, child: Text("Add"))
            ],
          )
      ),
    );
  }

  Widget buildDetailProduct(){
    final cates = CategoryProvider.of(context).cates;

    print(cates);
    if(cates.isEmpty){
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.note,size: 100,color: Colors.grey),
          Text('You did not have any category of products yet.',
            style: Theme.of(context).textTheme.headline5,)
        ],
      );
    }
    return ListView.builder(
        itemCount: cates.length,
        itemBuilder: (context,index){
          final cate = cates[index];
          return Padding(padding: EdgeInsets.fromLTRB(8, 2, 8, 2),
          child: Card(
              child: Dismissible(
              key: ValueKey(cate),
          background: Container(color: Colors.red),
          direction: DismissDirection.endToStart,
          onDismissed: (_){
          final controller = CategoryProvider.of(context);
          controller.deleteCate(cate);
          setState(() {

          });
          },
          child: ListTile(
          title: Text(cate.name),
          subtitle: Text(cate.availableMessage),
          trailing: Icon(Icons.more_vert),
          onTap: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (_)=> ProductDetailScreen(cate: cate,)));
          },
          ),
          ),
            shadowColor: Colors.indigo,
            elevation: 5.0,
          ) ,
          );
        });
  }



}
